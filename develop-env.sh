#!/bin/bash

echo "Necesitar installar GIT y correr Docker"

echo "Git clone api-demo en el directorio"
echo "Por favor remover /api-demo en caso de existir el directorio"
git clone -b develop https://git@bitbucket.org:jrodasc/demo-api.git ./app/api-demo


echo "Git clone web-demo en el directorio"
echo "Por favor remover /web-demo en caso de existir el directorio"
git clone -b develop https://git@bitbucket.org:jrodasc/web-demo.git ./app/web-demo

echo "Generar imagenes de api-demo y web-demo"
docker-compose -f docker-compose.yml -f develop.yml build --no-cache

echo "Iniciar servicio"
docker-compose -f docker-compose.yml -f develop.yml up